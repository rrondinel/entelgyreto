package pe.com.reto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.reto.services.RetoServices;

@RestController
@RequestMapping("/reto")
public class RetoController {
	
	@Autowired
	private RetoServices services;
	
	@GetMapping(path = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listar() {
		
		return ResponseEntity.status(HttpStatus.OK).body(this.services.getContactos());
	}

}
