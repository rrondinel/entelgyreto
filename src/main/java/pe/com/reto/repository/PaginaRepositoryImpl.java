package pe.com.reto.repository;

import java.util.Arrays;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import pe.com.reto.model.Pagina;

@Repository
public class PaginaRepositoryImpl implements PaginaRepository {

	/*@Autowired
	private */
	
	@Value("${service.externo}")
	private String urlServicio;
	
	@Override
	public Pagina getPagina() {
		// TODO Auto-generated method stub
		RestTemplate restTemplate= new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<Pagina> pagina=restTemplate.exchange(urlServicio, HttpMethod.GET, entity, Pagina.class);
		Pagina data=pagina.getBody();
		
		return data;
	}

}
