package pe.com.reto.repository;

import pe.com.reto.model.Pagina;

public interface PaginaRepository {
	
	Pagina getPagina();

}
