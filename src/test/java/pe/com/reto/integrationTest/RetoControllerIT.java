package pe.com.reto.integrationTest;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import pe.com.reto.model.Pagina;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RetoControllerIT {
	
	@Autowired
	TestRestTemplate testRestTemplate;
	
	ResponseEntity<Pagina> responseEntity;

	
	@Test
	public void getListar() {	
		responseEntity=testRestTemplate.getForEntity("/reto/listar", Pagina.class);
		assertTrue(!responseEntity.getBody().getData().isEmpty());
	}
	
	@Test
	public void responseOK() {
		responseEntity=testRestTemplate.getForEntity("/reto/listar", Pagina.class);
		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
	}

}
